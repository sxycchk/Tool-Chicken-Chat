from sqlalchemy import func
from sqlalchemy_serializer import SerializerMixin
from app import CONST
from app.Models.Base import Base
from app.Models.Model import ThRedPackRecord
from app import dBSession

class RedPackRecord(Base, ThRedPackRecord, SerializerMixin):
    
    def updateBestByRedPackId(self, red_package_id):
        subqry = dBSession.query(func.max(RedPackRecord.price)).filter(RedPackRecord.red_package_id == red_package_id,)
        return RedPackRecord().edit(RedPackRecord, {
            'status': CONST['REDPACKRECORDTATUS']['BEST']['value'],
        }, {
            RedPackRecord.red_package_id == red_package_id,
            RedPackRecord.price == subqry
        })